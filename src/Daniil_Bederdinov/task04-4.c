#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 5

void shuffle(char(*arr)[256], int n)
{
    srand(time(NULL));
    int index;
    int buff[N];

    for (int i = 0; i<n; i++)
        buff[i] = i;
    for (int i = n; i>0; i--)
    {
        index = rand() % i;
        printf("%s \n", arr[buff[index]]);
        arr[buff[index]][0] = 0;
        buff[index] = buff[i - 1];
    }
}

int main()
{
    char str[N][256];
    int i = 0;
    while (i < N)
    {
        fgets(str[i], 256, stdin);
        i++;
    }
    shuffle(str,N);
    return 0;
}